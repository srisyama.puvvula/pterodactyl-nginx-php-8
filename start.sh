#!/bin/ash

echo "Starting PHP-FPM..."
/usr/sbin/php-fpm81 --fpm-config "/home/container/php-fpm/php-fpm.conf" --daemonize

echo "Starting Nginx..."
/usr/sbin/nginx -c "/home/container/nginx/nginx.conf" -e "/home/container/error.log"
